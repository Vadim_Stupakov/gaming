from evdev import InputDevice, list_devices, ecodes
import logging


class DeviceManager:
    def __init__(self):
        self._mouse_events = {ecodes.EV_KEY, ecodes.EV_REL, ecodes.BTN_RIGHT, ecodes.BTN_LEFT}
        self._keyboard_events = {ecodes.EV_KEY, ecodes.KEY_W, ecodes.KEY_A, ecodes.KEY_D, ecodes.KEY_S}
        self._devices = [InputDevice(path) for path in list_devices()]

        self._mouse_devices = list()
        self._keyboard_devices = list()

        self._get_mouse_devices()

    def _get_mouse_devices(self):
        for device in self._devices:
            cap = list()
            for l in device.capabilities().values():
                cap.extend(l)

            cap = set(cap)

            if self._mouse_events.issubset(cap):
                self._mouse_devices.append(device)
            elif self._keyboard_events.issubset(cap):
                self._keyboard_devices.append(device)

    @property
    def keyboards(self):
        return self._keyboard_devices

    @property
    def mouses(self):
        return self._mouse_devices
