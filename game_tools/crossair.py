#!/usr/bin/env python3

from time import sleep

from Xlib import X, display

from threading import Thread
from enum import Enum


class Color(Enum):
    red = (65535, 0, 0)
    green = (0, 65535, 0)
    blue = (0, 0, 65535)


class Drawer:
    def __init__(self, color=Color.green):
        self._d = display.Display()
        screen = self._d.screen()
        self._window = screen.root
        colormap = screen.default_colormap
        color = colormap.alloc_color(*color.value)

        self._gc = self._window.create_gc(
            line_width=1,
            line_style=X.LineSolid,
            fill_style=X.FillOpaqueStippled,
            fill_rule=X.WindingRule,
            join_style=X.JoinMiter,
            foreground=color.pixel,
            background=screen.black_pixel,
            function=X.GXcopy,
            graphics_exposures=False,
            subwindow_mode=X.IncludeInferiors,
        )

    def draw_line(self, x0, y0, x1, y1):
        self._window.line(self._gc, x0, y0, x1, y1)
        self._d.flush()


class CrossHair(Drawer):
    def __init__(self, size: int = 10, delay=0.01):
        super().__init__()
        self._size = size
        self._delay = delay
        self._thread = Thread(target=self.draw_crosshair, daemon=True)

        geometry = self._window.get_geometry()
        x0, y0 = geometry._data["width"] // 2, geometry._data["height"] // 2
        horisontal = x0 - self._size, y0, x0 + self._size, y0
        vertical = x0, y0 - self._size, x0, y0 + self._size
        self._coords = horisontal, vertical

    def draw_crosshair_once(self):
        for coord in self._coords:
            self.draw_line(*coord)

    def draw_crosshair(self):
        while True:
            sleep(self._delay)
            self.draw_crosshair_once()


if __name__ == '__main__':
    crosshair = CrossHair(size=10, delay=0.001)
    crosshair.draw_crosshair()
