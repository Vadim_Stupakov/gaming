#!/usr/bin/env python3

import logging
from threading import Thread
from time import sleep

import Xlib
from evdev import InputEvent, InputDevice, ecodes

from .crossair import CrossHair
from .listener import Listener
from .utils import DeviceManager


class Helper:
    def __init__(self, delay=0.01):
        self._display = Xlib.display.Display()

        self._win_name_to_activate = "Counter-Strike".lower()

        self._delay = delay
        self._callbacks = []

        self._activated = True
        self._LB_pressed = False
        self._special_pressed = False

        self._device_mng = DeviceManager()

        self._delta_y_sum = 0
        self._delta_y = 2

        self._devices = self._device_mng.keyboards + self._device_mng.mouses

        self._callbacks.append(self._activate_helper)
        self._callbacks.append(self._clicks_handler)

        self._workers = [Thread(target=self._worker_crosshair, daemon=True),
                         Thread(target=self._worker_press_button, daemon=True),
                         Thread(target=self._check_window, daemon=True)]

        self._listener = Listener(callbacks=self._callbacks, devices=self._devices)

    def _check_window(self):
        while True:
            try:
                window = self._display.get_input_focus().focus
                wmname = str(window.get_wm_name())

                if self._win_name_to_activate in wmname.lower():
                    self._activated = True
                else:
                    self._activated = False

                sleep(self._delay)
            except:
                pass

    def _activate_helper(self, event: InputEvent, device: InputDevice):
        if event and event.type == ecodes.EV_KEY and event.code == ecodes.KEY_DELETE and event.value == 1:
            self._activated = not self._activated
            logging.debug("Keyboard: DEL pressed")

    def _clicks_handler(self, event, device: InputDevice):
        if event and self._activated and event.type == ecodes.EV_KEY and event.code == ecodes.BTN_LEFT:
            if event.value == 1:
                self._LB_pressed = True
                logging.debug("Mouse: LB pressed")
            elif event.value == 0:
                logging.debug("Mouse: LB released")
                self._LB_pressed = False

    def _worker_crosshair(self):
        crosshair = CrossHair()
        while True:
            if self._activated:
                crosshair.draw_crosshair_once()
            sleep(self._delay)

    def _special_press(self):
        if not self._special_pressed:
            self._special_pressed = True
            for dev in self._device_mng.keyboards:
                dev.write(ecodes.EV_KEY, ecodes.KEY_LEFTSHIFT, 1)
            sleep(self._delay)

    def _special_release(self):
        if self._special_pressed:
            self._special_pressed = False
            for dev in self._device_mng.keyboards:
                dev.write(ecodes.EV_KEY, ecodes.KEY_LEFTSHIFT, 0)
            sleep(self._delay)

    def _worker_press_button(self):
        while True:
            if self._LB_pressed:
                self._special_press()

                self._device_mng.keyboards[0].write(ecodes.EV_KEY, ecodes.KEY_0, 1)

                sleep(self._delay)

                self._device_mng.keyboards[0].write(ecodes.EV_KEY, ecodes.KEY_0, 0)

                self._device_mng.mouses[0].write(ecodes.EV_REL, ecodes.REL_Y, self._delta_y)
                self._delta_y_sum += self._delta_y

                logging.debug("Generated event")
            else:
                self._special_release()

                if self._delta_y_sum > 0:
                    self._device_mng.mouses[0].write(ecodes.EV_REL, ecodes.REL_Y, -self._delta_y_sum)
                    self._delta_y_sum = 0

            sleep(self._delay)

            for dev in self._devices:
                dev.write(ecodes.EV_SYN, 0, 1)
                dev.write(ecodes.EV_SYN, 0, 1)

    def start(self):
        for worker in self._workers:
            worker.start()

        self._listener.start()

    def join(self):
        for worker in self._workers:
            worker.join()

        self._listener.join()

    def run(self):
        self.start()
        self.join()
