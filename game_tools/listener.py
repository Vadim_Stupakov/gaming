#!/usr/bin/env python3

import evdev
import logging
from evdev import InputEvent, InputDevice, ecodes
from threading import Thread

from game_tools.utils import DeviceManager


class Listener:
    def __init__(self, callbacks: list, devices):
        if not isinstance(callbacks, list):
            raise ValueError

        self._callbacks = callbacks

        self._devs = devices

        self._threads = []

    def _listen_one(self, device):
        for event in device.read_loop():
            for fun in self._callbacks:
                fun(event, device)

    def _listen_all(self, devices):
        for device in devices:
            worker = Thread(target=self._listen_one, args=(device,), daemon=True)
            self._threads.append(worker)

    def start(self):
        self._listen_all(self._devs)
        for thread in self._threads:
            thread.start()

    def join(self):
        for thread in self._threads:
            thread.join()


if __name__ == '__main__':
    mouse = DeviceManager().mouses
    print(mouse)


    def callback(event: InputEvent, device: InputDevice):
        if event and event.type == evdev.ecodes.EV_KEY:
            if event.code == evdev.ecodes.BTN_LEFT:
                if event.value == 1:
                    print("Mouse: LB pressed")
                    device.write(ecodes.EV_KEY, ecodes.BTN_RIGHT, 1)
                elif event.value == 0:
                    print("Mouse: LB released")
                    device.write(ecodes.EV_KEY, ecodes.BTN_RIGHT, 0)


    c = Listener(devices=mouse, callbacks=[callback])
    c.start()
    c.join()
