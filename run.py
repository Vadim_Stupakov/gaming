import logging

from game_tools.game_helper import Helper

if __name__ == '__main__':
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    c = Helper()
    c.run()
